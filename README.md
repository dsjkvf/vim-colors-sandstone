# Sandstone colorscheme for Vim

## About

Sandstone is a (not so) minimal colorscheme for Vim editor built around different variations of [sandstone colours](https://i.imgur.com/2k2Dn1Y.png).

## Installation

Copy `sandstone.vim` to the `$VIMHOME/colors` directory, or simply use any plugin manager available.

## Screenshots

C:

![c](https://i.imgur.com/mGsJB1l.png)

Javascript:

![js](https://i.imgur.com/pNlvgTP.png)

R:
![r](https://i.imgur.com/nOHraLl.png)

Diff:

![diff](https://i.imgur.com/ynW9vIr.png)
