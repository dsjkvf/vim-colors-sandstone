" vim: set nowrap:

" HEADER

" Description:  Vim color scheme, sandstone.vim
" Author:       dsjkvf (dsjkvf@gmail.com)
" Maintainer:   dsjkvf (dsjkvf@gmail.com)
" Webpage:      https://bitbucket.org/dsjkvf/vim-colors-sandstone
" License:      MIT
" Version:      0.3

" MAIN

" Startup
hi clear
if exists("syntax_on")
    syntax reset
endif
let colors_name = "sandstone"

" Settings
" if no italics setting is forced via the global configuration variable
if !exists('g:sandstone_italic')
    " check if the terminfo entry declares support for italics
    if &term =~# 'italic'
        let g:sandstone_italic = 'italic'
    else
        let g:sandstone_italic = 'NONE'
    endif
endif

" Interface
hi Normal        cterm=NONE      gui=NONE      guibg=#efeae5 guifg=#72645f
" hi Normal        cterm=NONE      gui=NONE      guibg=#efeae5 guifg=#fffff0
set background=light
hi Terminal      cterm=NONE      gui=NONE      guibg=bg      guifg=NONE
hi ColorColumn   cterm=NONE      gui=NONE      guibg=#9f918c guifg=NONE
hi Conceal       cterm=NONE      gui=NONE      guibg=bg      guifg=#9f918c
hi Cursor        cterm=NONE      gui=NONE      guibg=#ff9955 guifg=#ffffff
hi CursorColumn  cterm=NONE      gui=NONE      guibg=#ffffff guifg=NONE
hi CursorLine    cterm=NONE      gui=NONE      guibg=#ffffff guifg=NONE
hi CursorLineNr  cterm=NONE      gui=NONE      guibg=#ffffff guifg=NONE
hi Directory     cterm=NONE      gui=NONE      guibg=bg      guifg=NONE
hi ErrorMsg      cterm=NONE      gui=NONE      guibg=#a7111d guifg=#ffe0e0
hi FoldColumn    cterm=NONE      gui=NONE      guibg=bg      guifg=#9f918c
" hi Folded        cterm=NONE      gui=NONE      guibg=#d2bdb7 guifg=#9f918c
hi Folded        cterm=NONE      gui=NONE      guibg=#e8dedb guifg=#9f918c
" hi Folded        cterm=NONE      gui=NONE      guibg=#eae0d6 guifg=#9f918c
hi IncSearch     cterm=NONE      gui=NONE      guibg=fg      guifg=#ffffff
hi LineNr        cterm=NONE      gui=NONE      guibg=bg      guifg=#d2bdb7
hi MatchParen    cterm=NONE      gui=NONE      guibg=bg      guifg=#ffffff
hi ModeMsg       cterm=NONE      gui=NONE      guibg=bg      guifg=NONE
hi MoreMsg       cterm=NONE      gui=NONE      guibg=bg      guifg=NONE
hi NonText       cterm=NONE      gui=NONE      guibg=bg      guifg=bg
hi Pmenu         cterm=NONE      gui=NONE      guibg=#ffffff guifg=NONE
hi PmenuSbar     cterm=NONE      gui=NONE      guibg=#ffffff guifg=NONE
hi PmenuSel      cterm=NONE      gui=NONE      guibg=#9f918c guifg=#ffffff
hi PmenuThumb    cterm=NONE      gui=NONE      guibg=#9f918c guifg=NONE
hi Question      cterm=NONE      gui=NONE      guibg=bg      guifg=NONE
hi Search        cterm=NONE      gui=NONE      guibg=#ffffff guifg=NONE
hi SignColumn    cterm=NONE      gui=NONE      guibg=bg      guifg=NONE
hi StatusLine    cterm=NONE      gui=NONE      guibg=#d2bdb7 guifg=NONE
hi! def link StatusLineTerm                    StatusLine
" hi StatusLineNC  cterm=NONE      gui=NONE      guibg=#eae0d6 guifg=bg
hi StatusLineNC cterm=NONE       gui=NONE      guibg=#d2bdb7 guifg=bg
hi! def link StatusLineTermNC                  StatusLineNC
hi! def link TabLine                           StatusLine
hi! def link TabLineFill                       StatusLine
hi TabLineSel    cterm=inverse   gui=inverse   guibg=NONE    guifg=NONE
hi Title         cterm=NONE      gui=NONE      guibg=bg      guifg=fg
hi Todo          cterm=underline gui=underline guibg=bg      guifg=NONE
hi Underlined    cterm=NONE      gui=NONE      guibg=bg      guifg=NONE
hi VertSplit     cterm=NONE      gui=NONE      guibg=bg      guifg=#9f918c
hi Visual        cterm=NONE      gui=NONE      guibg=#ffffff guifg=NONE
hi VisualNOS     cterm=NONE      gui=NONE      guibg=#ffffff guifg=NONE
" hi WarningMsg    cterm=bold      gui=bold      guibg=NONE    guifg=#ff0000
hi WarningMsg    cterm=NONE      gui=NONE      guibg=#f9ffa3 guifg=#7b6a3d
" hi WildMenu      cterm=NONE      gui=NONE      guibg=#e8ffd1 guifg=#408000
hi! def link WildMenu Cursor

" Syntax
hi Comment       cterm=italic    gui=italic    guibg=bg      guifg=#d2bdb7
hi String        cterm=NONE      gui=NONE      guibg=bg      guifg=#5f87af
hi Todo          cterm=underline gui=underline guibg=bg      guifg=NONE
hi! def link Boolean                           String
hi! def link Float                             String
hi! def link Number                            String
hi! def link Constant                          String
hi! def link Function                          Normal
hi! def link Identifier                        Normal
hi! def link PreProc                           Normal
hi! def link Special                           Normal
hi! def link SpecialKey                        String
hi! def link Statement                         Normal
hi! def link Type                              Normal

" for Diff mode
hi DiffAdd       cterm=NONE      gui=NONE      guibg=#e8ffd1 guifg=#408000
hi! def link DiffChange                        WarningMsg
hi! def link DiffDelete                        ErrorMsg
hi DiffText      cterm=NONE      gui=NONE      guibg=#e0f3ff guifg=#5f87af
hi! def link DiffCommit                        diffChange
hi! def link DiffCommitFile                    diffChange

" for Diff
hi! def link diffAdded                         DiffAdd
hi! def link diffChanged                       DiffChange
hi! def link diffRemoved                       DiffDelete
hi! def link diffComment                       Comment
hi! def link diffFile                          Comment
hi! def link diffNewFile                       Comment
hi! def link diffLine                          Comment
hi! def link diffIndexLine                     Comment

" for Help
hi! def link helpHyperTextJump                 String

" for HTML
hi! def link htmlLink                          Normal
hi! def link htmlComment                       Comment
hi! def link htmlCommentPart                   Comment

" for Javascript
hi! def link jsGlobalObjects                   Normal

" for Markdown
hi mkdBlockquote cterm=italic    gui=italic    guibg=bg      guifg=#9f918c
hi! def link markdownHeadingDelimiter          Title
hi! def link markdownHeadingRule               Title
hi! def link markdownLinkText                  Normal
hi! def link mkdCode                           String
hi! def link mkdComment                        Comment
hi! def link mkdLink                           String
hi! def link mkdURL                            Comment
hi! def link mkdInlineURL                      String

" for Mail
hi mailQuoted1   cterm=italic    gui=italic    guibg=bg      guifg=#949494
hi mailQuoted2   cterm=italic    gui=italic    guibg=bg      guifg=#a8a8a8
hi mailQuoted3   cterm=italic    gui=italic    guibg=bg      guifg=#bcbcbc
hi mailQuoted4   cterm=italic    gui=italic    guibg=bg      guifg=#c6c6c6
hi! def link mailEmail                         String
hi! def link mailURL                           String
hi! def link mailHeader                        mailQuoted3
hi! def link mailHeaderKey                     mailQuoted3
hi! def link mailSubject                       Title

" for Spelling
hi SpellBad      cterm=undercurl gui=undercurl guibg=bg      guifg=NONE
hi SpellCap      cterm=undercurl gui=undercurl guibg=bg      guifg=NONE
hi SpellLocal    cterm=undercurl gui=undercurl guibg=bg      guifg=NONE
hi SpellRare     cterm=undercurl gui=undercurl guibg=bg      guifg=NONE

" for Startify
hi! def link StartifyPath                      Comment
hi! def link StartifySection                   Title


" APPENDIX

" #408000     // green                // DiffAdd DiffAdded
" #5f87af     // blue                 // String Boolean Float Number Constant SpecialKey helpHyperTextJump mkdCode mkdLink mkdInlineURL mailEmail mailURL DiffText
" #72645f     // sandstone            // Normal Function Identifier PreProc Special Statement Type htmlLink jsGlobalObjects markdownLinkText
" #7b6a3d     // yellow (d)           // WarningMsg DiffChange
" #949494     // gray                 // mailQuoted1
" #9f918c     // sandstone (ll)       // ColorColumn Conceal FoldColumn Folded PmenuSel PmenuThumb VertSplit mkdBlockquote // not linked
" #a7111d     // red                  // ErrorMsg DiffDelete
" #a8a8a8     // gray (l)             // mailQuoted2
" #bcbcbc     // gray (ll)            // mailQuoted3
" #c6c6c6     // gray (lll)           // mailQuoted4
" #d2bdb7     // sandstone (lll)      // Comment LineNr StatusLine StatusLineNC
" #e0f3ff     // blue (l)             // DiffText
" #e8dedb     // sandstone (llll)     // Folded
" #e8ffd1     // green (l)            // DiffAdd DiffAdded
" #eae0d6     // sandstone (lllll)    // 
" #efeae5     // sandstone (llllll)   // Background
" #f9ffa3     // yellow               // WarningMsg DiffChange
" #ff9955     // orange               // Cursor WildMenu
" #ffe0e0     // red (l)              // ErrorMsg
" #ffffff     // white                // Cursor CursorColumn CursorLine CursorLineNr IncSearch MatchParen Pmenu PmenuSbar PmenuSel Search Visual VisualNOS
